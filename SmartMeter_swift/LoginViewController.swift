//
//  LoginViewController.swift
//  SmartMeter_swift
//
//  Created by  on 14-7-14.
//  Copyright (c) 2014 qkong. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var username: UITextField
    
    @IBOutlet weak var password: UITextField
    
    @IBOutlet var bgView: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bgView.layer.cornerRadius = 5.0
        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func loginAction(sender: AnyObject) {
        if self.username.text == ""&&self.password.text=="" {
            self.performSegueWithIdentifier("mainVC", sender: nil)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool{
        textField.resignFirstResponder()
        if textField == self.username{
            self.password.becomeFirstResponder()
        }
        return true;
    }
    
    override func touchesBegan(touches: NSSet!, withEvent event: UIEvent!) {
        self.view.endEditing(true)
    }

    // #pragma mark - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        
    }


}
