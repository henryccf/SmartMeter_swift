//
//  MainViewController.swift
//  SmartMeter_swift
//
//  Created by 江苏宏创 on 14-7-14.
//  Copyright (c) 2014年 qkong. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet var lastBtn: UIButton
    
    
    @IBOutlet var scrollView: UIScrollView


    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollView.frame = UIScreen.mainScreen().bounds
        NSLog("scrollview--->%@", NSStringFromCGRect(self.scrollView.frame))
        self.scrollView.contentSize = CGSizeMake(screenWidth, self.lastBtn.frame.origin.y+100)
        NSLog("scrollview--->%@", NSStringFromCGSize(self.scrollView.contentSize))
        self.scrollView.scrollEnabled = true
        self.initNavi()
        // Do any additional setup after loading the view.
    }
    
    
    func initNavi(){
        var logoutBtn :UIButton = UIButton(frame:CGRectMake(5, 10, 55, 33))
        logoutBtn.setTitle("注销", forState: UIControlState.Normal)
        logoutBtn.setBackgroundImage(UIImage(named: "title_back_bg_n.png"), forState: UIControlState.Normal)
        logoutBtn.setBackgroundImage(UIImage(named: "title_back_bg_p.png"), forState: UIControlState.Highlighted)
        logoutBtn.addTarget(self, action: "logoutAction", forControlEvents: UIControlEvents.TouchUpInside)
        var logoutBtnItem :UIBarButtonItem = UIBarButtonItem(customView: logoutBtn)
        self.navigationItem.leftBarButtonItem = logoutBtnItem
    }
    
    func logoutAction(){
        self.dismissViewControllerAnimated(true, completion: {})
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func chooseMenu(sender: UIButton) {
        switch(sender.tag){
            case 10:
                //分组抄表
                self.performSegueWithIdentifier("groupMeterOrTableMaintenance", sender: ["isGroupMeterVC":true])
            case 11:
                //指定抄表
            self.performSegueWithIdentifier("queryMeter", sender: nil)
            case 12:
                //未抄查询
            self.performSegueWithIdentifier("unMeterQuery", sender: nil)
            case 13:
                //气表维护
            self.performSegueWithIdentifier("groupMeterOrTableMaintenance", sender: ["isGroupMeterVC":false])
            case 14:
                //统计
            self.performSegueWithIdentifier("statistics", sender: nil)
            default:
                println("\(sender.tag)")
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue?, sender: AnyObject?) {
        var userLocationVC : UserLocationViewController = segue?.destinationViewController as UserLocationViewController
        if segue?.identifier == "groupMeterOrTableMaintenance"{
            var userLocationVC : UserLocationViewController = segue!.destinationViewController as UserLocationViewController
            if sender!["isGroupMeterVC"] as Bool == true {
                userLocationVC.isGroupMeterVC = true;
            }else{
                userLocationVC.isGroupMeterVC = false;
            }
        }
    }

}
