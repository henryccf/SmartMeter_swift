//
//  UserLocationViewController.swift
//  SmartMeter_swift
//
//  Created by  on 14-7-14.
//  Copyright (c) 2014 qkong. All rights reserved.
//

import UIKit

class UserLocationViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource{
    
    @IBOutlet var locationPicker: UIPickerView
    
    var isGroupMeterVC : Bool?
    
    let nameList:[String] = ["community","building","unit"]
    
    let testData : Dictionary<String,[String]> = [
        "community":["甘家巷","炼油厂","小区3","小区4","小区5","小区6","小区7","小区8","小区9","小区10"],
        "building":["1#","2#","3#","4#","5#","6#","7#","8#","9#"],
        "unit":["1单元","2单元","3单元","4单元","5单元","6单元","7单元","8单元"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initNavi()
        // Do any additional setup after loading the view.
    }
    
    func initNavi(){
        
        // 左边返回按钮
        var backBtn : UIButton = UIButton(frame: CGRectMake(5, 10, 55, 33))
        backBtn.setTitle("返回", forState: UIControlState.Normal)
        backBtn.setBackgroundImage(UIImage(named: "title_back_bg_n.png"), forState: UIControlState.Normal)
        backBtn.setBackgroundImage(UIImage(named: "title_back_bg_n.png"), forState: UIControlState.Highlighted)
        backBtn.addTarget(self, action: "backAction", forControlEvents: UIControlEvents.TouchUpInside)
        var backBtnItem : UIBarButtonItem = UIBarButtonItem(customView: backBtn)
        self.navigationItem.leftBarButtonItem = backBtnItem
        
        //右边确定按钮
        var sureBtn : UIButton = UIButton(frame: CGRectMake(5, 10, 55, 33))
        sureBtn.setTitle("确认", forState: UIControlState.Normal)
        sureBtn.setBackgroundImage(UIImage(named: "title_back_bg_n.png"), forState: UIControlState.Normal)
        sureBtn.setBackgroundImage(UIImage(named: "title_back_bg_P.png"), forState: UIControlState.Highlighted)
        sureBtn.addTarget(self, action: "sureAction", forControlEvents: UIControlEvents.TouchUpInside)
        var sureBtnItem : UIBarButtonItem = UIBarButtonItem(customView: sureBtn)
        self.navigationItem.rightBarButtonItem = sureBtnItem
    }
    
    func backAction(){
        self.navigationController.popViewControllerAnimated(true)
    }
    
    func sureAction(){
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView!) -> Int {
        return 3
    }
    
    func pickerView(pickerView: UIPickerView!, numberOfRowsInComponent component: Int) -> Int {
        return testData[nameList[component]]!.count
    }
    
    func pickerView(pickerView: UIPickerView!, titleForRow row: Int, forComponent component: Int) -> String! {
        var locationArr : [String] = testData[nameList[component]]!
        return locationArr[row]
    }
    /*
    // #pragma mark - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
